'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getRouter;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _mkdirp = require('mkdirp');

var _httpErrors = require('http-errors');

var _httpErrors2 = _interopRequireDefault(_httpErrors);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-next-line no-unused-vars
var dbg = (0, _debug2.default)('ddd-create');

function getRouter(_ref) {
  var config = _ref.config,
      Path = _ref.Path;

  var router = _express2.default.Router();
  router.post('/', function (req, res, next) {
    var path = Path.from(req.body.path);
    try {
      (0, _fs.statSync)(path.asPath());
      var msg = 'that path already exists: ' + path.asPath();
      return next((0, _httpErrors2.default)(500, msg));
    } catch (err) {
      if (err.code != 'ENOENT') throw err;
    }
    try {
      (0, _mkdirp.sync)(path.parse().dir);
    } catch (err) {
      var _msg = 'some error occurred trying to create path: ' + path.parse().dir;
      return next((0, _httpErrors2.default)(500, _msg, { stack: err.stack }));
    }
    try {
      (0, _fs.writeFileSync)(path.asPath(), '', 'utf8');
    } catch (err) {
      var _msg2 = 'some error occurred trying to write: ' + path.asPath();
      return next((0, _httpErrors2.default)(500, _msg2, { stack: err.stack }));
    }

    res.status(201).end();
  });
  return router;
}