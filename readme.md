## dotdotdown-create-file

A dotdotdown plugin to create files

## pug runtime

Presently, this is bundled in each component adding ~6k to the raw bundle size. It's not ideal but not a big problem given that dotdotdown is designed to be used locally. I'm not sure what a nice resolution might be so I've opened [an issue here](https://github.com/pugjs/pug-loader/issues/115).

## managing built resources

The usual strategy for managing built resources is to set a .gitignore file to ignore them, and then a .npmignore to ignore your source and include your built resources. That way your npm package includes the built files, ready for use when someone installs it.
For the time being I'm not planning to host plugins on npm, which means installing them from a git repo, so we really want the built resources in the repo. Often this isn't a good idea because version control systems typically don't work well with built / compressed / minified files. The workaround is to use git LFS.
It's pretty easy, there's a good guide [on gitlab](https://about.gitlab.com/2017/01/30/getting-started-with-git-lfs-tutorial/).

Suppose you wanted to make a plugin, cloned this repo, then `rm -rf .git`.

Once you have local git lfs support (see linked guide), then you'd just:

 * `git lfs track dist/index.js`
 * `git add -f dist/index.js`
 * `git lfs track dist/component.js`
 * `git add -f dist/component.js`
 * `git add .gitattributes`
 * `git commit -m "track built resources"`
