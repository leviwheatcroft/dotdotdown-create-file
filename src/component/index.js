/* eslint-env browser */
/* global $ dotdotdown */

import styles from './styles'
import layout from './layout'

const {
  Component,
  Alert,
  Uri,
  registerPlugin
} = dotdotdown

export default class Create extends Component {
  constructor () {
    super(layout, styles)
    this.uri = new Uri()
  }
  render () {
    super.render({ uri: this.uri })
    this.el.on('hidden.bs.modal', () => this.el.remove())
    Alert.load($('.alert-container', this.el))
    return this
  }
  keypressPath (event) {
    if (event.which === 13) this.clickCreate()
  }
  async clickCreate () {
    $('button#create').addClass('loading')
    let path = `${this.uri.dir}${$('input[name=path]').val()}`
    try {
      await $.ajax({
        url: '/ddd/create-file',
        method: 'POST',
        data: JSON.stringify({ path }),
        contentType: 'application/json',
        processData: false
      })
      setTimeout(() => window.location.href = path, 500)
    } catch (err) {
      $('.alert-container', this.el).component().addAlert({
        dismissable: true,
        context: 'danger',
        content: err.responseJSON.err.message
      })
      console.log(err.responseJSON.err)
    }
    $('button#create').removeClass('loading')
  }
  submitForm (event) {
    event.preventDefault()
  }
  static load () {
    const component = new Create()
    const description = 'Create'
    const classes = ['far', 'fa-plus-square']
    const onClick = function () {
      component.render()
      .el.appendTo($('body'))
      .modal('show')
    }
    $('nav').component().addButton(description, classes, onClick)
  }
}

registerPlugin(Create)
