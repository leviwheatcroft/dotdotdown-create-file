/**
 * export a router
 *
 * if the plugin doesn't need a router, just export `false` a la:
 * `export default false`
 */

import express from 'express'
import {
  writeFileSync,
  statSync
} from 'fs'
import {
  sync as mkdirpSync
} from 'mkdirp'
import httpErrors from 'http-errors'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-create')

export default function getRouter ({ config, Path }) {
  const router = express.Router()
  router.post('/', function (req, res, next) {
    const path = Path.from(req.body.path)
    try {
      statSync(path.asPath())
      const msg = `that path already exists: ${path.asPath()}`
      return next(httpErrors(500, msg))
    } catch (err) {
      if (err.code != 'ENOENT') throw err
    }
    try {
      mkdirpSync(path.parse().dir)
    } catch (err) {
      const msg = `some error occurred trying to create path: ${path.parse().dir}`
      return next(httpErrors(500, msg, { stack: err.stack }))
    }
    try {
      writeFileSync(path.asPath(), '', 'utf8')
    } catch (err) {
      const msg = `some error occurred trying to write: ${path.asPath()}`
      return next(httpErrors(500, msg, { stack: err.stack }))
    }

    res.status(201).end()
  })
  return router
}
